# Generated by Django 4.1.2 on 2022-10-19 23:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0002_receipt_date"),
    ]

    operations = [
        migrations.RenameField(
            model_name="receipt",
            old_name="purshaser",
            new_name="purchaser",
        ),
        migrations.AlterField(
            model_name="account",
            name="number",
            field=models.CharField(max_length=20),
        ),
    ]
