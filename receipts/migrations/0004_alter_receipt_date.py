# Generated by Django 4.1.2 on 2022-10-20 06:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0003_rename_purshaser_receipt_purchaser_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(null=True, verbose_name="%m/%d/%y"),
        ),
    ]
