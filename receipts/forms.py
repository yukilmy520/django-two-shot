from django import forms
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt   ##retreive fields from model class
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class CategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory  ##retreive fields from model class
        fields = [
            "name"
        ]

class AccountForm(forms.ModelForm):
    class Meta:
        model = Account  ##retreive fields from model class
        fields = [
            "name"
        ]
